﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using Excel = Microsoft.Office.Interop.Excel;
namespace QV_CuringStopReport
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Gipur Production דוח אוטומטי המבוסס על דוח התקלות מ 
            // נבנה ע"י רן הויכמן - 12/12/2016  למטרת מודל בקליקוויו 
            HMIStopReport();
        }

        private void HMIStopReport()
        {

            // שינויים מתאריך 24.11.2016 ע"י רן הויכמן
            // עדכון אופן שליפת מק"ט נכון 
            Cursor.Current = Cursors.WaitCursor;

            string Con_Str;
            string StrSql = "";
            int i = 2;
            int[] ArrErrCode = new int[20];
            ArrErrCode[0] = 900266; ArrErrCode[1] = 900267; ArrErrCode[2] = 900332; ArrErrCode[3] = 900333; ArrErrCode[4] = 900334; ArrErrCode[5] = 900335;
            ArrErrCode[6] = 900460; ArrErrCode[7] = 900461; ArrErrCode[8] = 900462; ArrErrCode[9] = 900463; ArrErrCode[10] = 900464; ArrErrCode[11] = 900941; ArrErrCode[12] = 900945;
            ArrErrCode[13] = 900798; ArrErrCode[14] = 900799; ArrErrCode[15] = 901155; ArrErrCode[16] = 901310; ArrErrCode[17] = 901311; ArrErrCode[18] = 903192; ArrErrCode[19] = 904429;

            int PressCount = 0;
            string[,] ArrPress;
            // ArrMac[0, 0] --> מכונה            
            // ArrMac[1, 0] --> תאריך שעה

            // AS400 התקשרות ל               
            Con_Str = "Dsn=as400;uid=as400;pwd=as400";
            DBConn DBC;
            DBC = new DBConn();
            DBC.Initialize_Conn(Con_Str);

            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook, xlWorkBook1;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            // עדכון מתאריך 29/01/2017
            // הוספת העתקת קובץ ה 0-1 לקליקוויו
            if (File.Exists(@"T:\AA_TAPI\A_Boker\TUHNOT\TfusatMahbesimOtom.xlsm"))
            {
                if (File.Exists(@"\\qlikview\e$\Excel Files\Faults\01.xlsm"))
                {
                    File.Delete(@"\\qlikview\e$\Excel Files\Faults\01.xlsm");
                    File.Copy(@"T:\AA_TAPI\A_Boker\TUHNOT\TfusatMahbesimOtom.xlsm", @"\\qlikview\e$\Excel Files\Faults\01.xlsm");    
                }
            }
            // עדכונים לתאריך 15/12/2016 ע"י רן הויכמן
            // עדכון הדוח במקום בנייתו מחדש כל יום. הוספת נתונים של 24 שעות בכל ריצה לתחתית הדוח
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(@"\\qlikview\e$\Excel Files\Faults\Faults.xlsx", 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //xlWorkBook = xlApp.Workbooks.Open(@"C:\C#\Faults.xlsx");
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // וידוא ריצה אחת ביום בלבד
            DateTime lastModified = System.IO.File.GetLastWriteTime(@"\\qlikview\e$\Excel Files\Faults\Faults.xlsx");
            if (lastModified.Day < DateTime.Now.Day)
            {
                /////////////////////////////////////////////
                Excel.Range start = xlWorkSheet.Range["A1"];
                Excel.Range bottom = xlWorkSheet.Range["A" + (xlWorkSheet.UsedRange.Rows.Count + 1)];
                Excel.Range end = bottom.End[Excel.XlDirection.xlUp];
                Excel.Range column = xlWorkSheet.Range[start, end];
                i = end.Row + 1;

                string LastPress = "";
                string StopStt = "0";
                string LastTak = "";
                string TakCode = "";
                string TakDesc = "";
                string EmpNo = "";
                string EmpName = "";
                //string Prod = "";
                //string Teken = "";
                //string Weight = "";
                //string TakTime = "";
                //string LostWeight = "";
                string newtime = "0";

                bool FlgBeforDt = false;  // דגל תקלה התחילה לפני תחילת דוח
                DateTime StartStopDT = DateTime.Now;

                // שלפית סה"כ מכבשים
                StrSql = "select count(distinct lpress) " +
                         "from RZPALI.TAXILOGP " +
                          "where LTIMESTAMP <= '" + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd-06.30.00.000000") + "' ";
                // "where LTIMESTAMP <= ''2016-01-01-06.30.00.000000' "' ";

                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    PressCount = DBC.Rs1.GetInt32(0);
                }
                DBC.Rs1.Close();


                ArrPress = new string[2, PressCount];

                // שליפת דיווח אחרון למכונה לפני תאריך תחילת הדוח
                // שלפית סה"כ מכבשים
                StrSql = "select LPRESS,max(LTIMESTAMP),WDEPT " +
                         "from RZPALI.TAXILOGP " +
                         "Left join bpcsfv30.lmhl01 on LPRESS=MHMACH " +
                         "left join bpcsfv30.lwkl01 on mhwrkc=wwrkc " +
                         "where LTIMESTAMP <= '" + DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd-06.30.00.000000") + "' and MHMACH is not null and WDEPT in (161,162) ";
                //if (comboB_Press.Text.Trim() != "")
                //    StrSql += "and LPRESS='" + comboB_Press.Text.Trim() + "' ";
                StrSql += "group by LPRESS,WDEPT ";
                StrSql += "order by LPRESS,WDEPT ";
                DBC.Q_Run1(StrSql);
                while (DBC.Rs1.Read())
                {
                    // שליפת דיווחים למכונה החל מתאריך דיווח אחרון למכונה עד סיום תקופת הדוח  
                    StrSql = "select LPRESS,LTIMESTAMP,LEMP,value(PRATI,'*'),value(FAMILY,'*'),LACT,LTAK,IDESC " +
                         "from RZPALI.TAXILOGP left join " +
                              "ISUFKV.ISAVL10  on LEMP=substring(OVED,3,5) and MIFAL='01' left join " +
                              "BPCSFV30.IIML01 on IPROD='TAK-161'||LTAK " +
                         "where LTIMESTAMP between '" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "' and " +
                                                  // קביעת ריצה עד תאריך נוכחי
                                                  "'" + DateTime.Now.ToString("yyyy-MM-dd-06.30.00.999999") + "' and LPRESS='" + DBC.Rs1.GetString(0).Trim() + "'";
                    // "'" + DTPicker_To.Value.ToString("yyyy-MM-dd-HH.mm.ss.999999") + "' and LPRESS='" + DBC.Rs1.GetString(0).Trim() + "'";
                    //if (radioB_Stop.Checked)
                    //    if (comboB_Stop.Text.Trim() != "")
                    //        StrSql += "and LTAK='" + comboB_Stop.Text.Trim().Substring(0, 2) + "' ";
                    StrSql += "order by LTIMESTAMP";
                    DBC.Q_Run2(StrSql);
                    while (DBC.Rs2.Read())
                    {
                        if (LastPress == "") // אתחול מכבש בתחילת ריצה
                            LastPress = DBC.Rs2.GetString(0).Trim(); // מכבש                    

                        if (DBC.Rs2.GetString(5).Trim() == "4" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "0")
                        {   // בדיקה אם דיווח תקלה + אותו מכבש + ולא היה דיווח תקלה                        
                            // בדיקה אם דיווח תקלה אחרי תחילת דוח                        
                            if (DBC.Rs2.GetDateTime(1) >= DateTime.Now.AddDays(-1))
                            {
                                StartStopDT = DBC.Rs2.GetDateTime(1); // שעת התחלה
                                FlgBeforDt = false;
                            }
                            else // אם דיווח תקלה לפני תחילת דוח אז זמן תחילת תקלה הוא מתחילת הדוח
                            {
                                StartStopDT = DateTime.Now.AddDays(-1);
                                FlgBeforDt = true;
                            }
                            LastTak = DBC.Rs2.GetString(6).Trim();
                            TakCode = DBC.Rs2.GetString(6).Trim();
                            TakDesc = DBC.Rs2.GetString(7).Trim();
                            EmpNo = DBC.Rs2.GetString(2).Trim();
                            if (!DBC.Rs2.IsDBNull(3) && !DBC.Rs2.IsDBNull(4))
                                EmpName = DBC.Rs2.GetString(3).Trim() + " " + DBC.Rs2.GetString(4).Trim();
                            StopStt = "1";
                        }
                        else
                        {
                            if (DBC.Rs2.GetString(5).Trim() == "4" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "1" && StartStopDT < DBC.Rs2.GetDateTime(1))
                            {   // מקרה של תקלה סוגרת תקלה
                                // בדיקה אם דיווח תקלה + אותו מכבש + תאריך דיווח גדול מתאריך תחלת תקלה
                                try
                                {
                                    xlWorkSheet.Cells[i, 1] = DBC.Rs2.GetString(0).Trim(); // מכבש
                                    xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה                            
                                    xlWorkSheet.Cells[i, 3] = DateTime.Parse(DBC.Rs2.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss")); // שעת סיום
                                    xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes); // סה''כ זמן
                                    xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                                    xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                                    xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                                    xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה   
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(e.Message + "   " + e.ToString());
                                    throw;
                                }


                                //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                                //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "from rzpali.taxilogp " +
                                //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "order by ltimestamp ";


                                StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                         "from rzpali.taxilogp " +
                                         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                         "order by ltimestamp ";
                                DBC.Q_Run3(StrSql);
                                string ProdBefore = "";
                                string ProdAfter = "";
                                string Selected = "";
                                while (DBC.Rs3.Read())
                                {
                                    if (ProdBefore == "")
                                    {
                                        ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                                    }
                                    else
                                    {
                                        ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                                    }
                                }
                                //בחירת הפריט שישובץ
                                if (ProdAfter == "")
                                {
                                    // שינויים מתאריך 28.11.2016
                                    // ובמקום בדיקה מול השיבוץ mcovip ביטול בדיקה מול  
                                    //StrSql = "select  omach, OPRIT,oadif " +
                                    //          "from rzpali.mcovil13 " +
                                    //          "where ODEPW in (161,162) and ODATE =" + StartStopDT.ToString("1yyMMdd") + "  and omade<oplan and OSHIFT ='1' and OMACH ='" + DBC.Rs1.GetString(0).Trim() + "'  and oadif=1 " +
                                    //          "order by omach, oshift, oadif ";
                                    StrSql = "select izprod,izqreq,izqprd,IZREQ " +
                                        "from bpcsfali.shmal01 " +
                                        "where IZDEPT='" + DBC.Rs1.GetValue(2).ToString().Trim() + "' and  IZMACH='" + DBC.Rs1.GetValue(0).ToString().Trim() + "' ";
                                    DBC.Q_Run3(StrSql);
                                    if (DBC.Rs3.Read())
                                    {
                                        ProdAfter = DBC.Rs3.GetValue(0).ToString();
                                    }
                                }
                                if (ProdBefore == ProdAfter)
                                {
                                    xlWorkSheet.Cells[i, 9] = ProdBefore;
                                    Selected = ProdBefore;
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i, 9] = ProdAfter;
                                    Selected = ProdAfter;
                                }

                                StrSql = "select  RMPROD,rmtkn,ICSCP1,insiz " +
                               "from  bpcsfali.frtml01 " +
                               "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                               "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                               "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                                DBC.Q_Run3(StrSql);
                                while (DBC.Rs3.Read())
                                {
                                    try
                                    {
                                        if (DBC.Rs3.GetValue(0).ToString().Trim() == Selected.Trim())
                                        {
                                            xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(1).ToString().Trim();
                                            xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(2), 2);
                                            xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(1) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                            xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(1), 2); // משקל שאבד        
                                            if (!DBC.Rs3.IsDBNull((3)))
                                            {
                                                xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(3).Trim();
                                            }

                                            newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        MessageBox.Show(e.Message + "   " + e.ToString());
                                        throw;
                                    }


                                }

                                StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                                DBC.Q_Run3(StrSql);
                                if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                                {
                                    xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i, 14] = "1";
                                }


                                ///////////////////////////////////////////////////////////////////////
                                LastTak = DBC.Rs2.GetString(6).Trim();
                                TakCode = DBC.Rs2.GetString(6).Trim();
                                TakDesc = DBC.Rs2.GetString(7).Trim();
                                EmpNo = DBC.Rs2.GetString(2).Trim();
                                if (!DBC.Rs2.IsDBNull(3) && !DBC.Rs2.IsDBNull(4))
                                    EmpName = DBC.Rs2.GetString(3).Trim() + " " + DBC.Rs2.GetString(4).Trim();
                                StopStt = "1";
                                if (FlgBeforDt)
                                {   // Cell Back Color
                                    chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                                    chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                                }
                                StartStopDT = DBC.Rs2.GetDateTime(1); // שעת התחלה
                                FlgBeforDt = false;
                                i++;
                            }
                            else if (DBC.Rs2.GetString(5).Trim() == "1" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "1")
                            {   // מקרה של תחילת גיפור
                                // בדיקה אם דיווח תקלה + אותו מכבש + תאריל דיווח גדול מתאריך תחלת תקלה
                                try
                                {
                                    xlWorkSheet.Cells[i, 1] = DBC.Rs2.GetString(0).Trim(); // מכבש
                                    xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה
                                    xlWorkSheet.Cells[i, 3] = DateTime.Parse(DBC.Rs2.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss")); // שעת סיום
                                    xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes); // סה''כ זמן
                                    xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                                    xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                                    xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                                    xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה  
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(e.Message + "   " + e.ToString());
                                    throw;
                                }

                                //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                                //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "from rzpali.taxilogp " +
                                //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "order by ltimestamp ";
                                StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                        "from rzpali.taxilogp " +
                                        "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                        "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                        "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                        "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                        "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                        "order by ltimestamp ";
                                DBC.Q_Run3(StrSql);
                                string ProdBefore = "";
                                string ProdAfter = "";
                                string Selected = "";
                                while (DBC.Rs3.Read())
                                {
                                    if (ProdBefore == "")
                                    {
                                        ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                                    }
                                    else
                                    {
                                        ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                                    }
                                }
                                // בחירת הפריט שישובץ
                                //if (ProdAfter == "")
                                //{
                                //    StrSql = "select  omach, OPRIT,oadif " +
                                //              "from rzpali.mcovil13 " +
                                //              "where ODEPW in (161,162) and ODATE =1161108  and omade<oplan and OSHIFT ='1' and OMACH ='301'  and oadif=1 " +
                                //              "order by omach, oshift, oadif ";
                                //    DBC.Q_Run3(StrSql);
                                //    if (DBC.Rs3.Read())
                                //    {
                                //        ProdAfter = DBC.Rs3.GetValue(1).ToString();
                                //    }
                                //}
                                if (ProdBefore == ProdAfter)
                                {
                                    xlWorkSheet.Cells[i, 9] = ProdBefore;
                                    Selected = ProdBefore;
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i, 9] = ProdAfter;
                                    Selected = ProdAfter;
                                }

                                StrSql = "select  RMPROD,rmtkn,ICSCP1,insiz " +
                               "from  bpcsfali.frtml01 " +
                               "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                               "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                               "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                                DBC.Q_Run3(StrSql);
                                while (DBC.Rs3.Read())
                                {
                                    try
                                    {
                                        if (DBC.Rs3.GetValue(0).ToString().Trim() == Selected.Trim())
                                        {
                                            xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(1).ToString().Trim();
                                            xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(2), 2);
                                            xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(1) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                            xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(1), 2); // משקל שאבד        
                                            xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(3).Trim();
                                            newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        MessageBox.Show(e.Message + "   " + e.ToString());
                                        throw;
                                    }


                                }

                                StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                                DBC.Q_Run3(StrSql);
                                if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                                {
                                    xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i, 14] = "1";
                                }
                                ///////////////////////////////////////////////////////////////////////
                                StopStt = "0";
                                LastTak = "";
                                TakCode = "";
                                TakDesc = "";
                                EmpNo = "";
                                EmpName = "";
                                if (FlgBeforDt)
                                {   // Cell Back Color
                                    chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                                    chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                                }
                                i++;
                            }
                            else if (DBC.Rs2.GetString(5).Trim() == "0" && DBC.Rs2.GetString(0).Trim() == LastPress && StopStt == "1")
                            {   // מקרה של תחילת גיפור
                                // בדיקה אם דיווח תקלה + אותו מכבש + תאריל דיווח גדול מתאריך תחלת תקלה
                                try
                                {
                                    xlWorkSheet.Cells[i, 1] = DBC.Rs2.GetString(0).Trim(); // מכבש
                                    xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה
                                    xlWorkSheet.Cells[i, 3] = DateTime.Parse(DBC.Rs2.GetDateTime(1).ToString("dd/MM/yyyy HH:mm:ss")); // שעת סיום
                                    xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes); // סה''כ זמן
                                    xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                                    xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                                    xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                                    xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה 
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(e.Message + "   " + e.ToString());
                                    throw;
                                }


                                //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                                //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "from rzpali.taxilogp " +
                                //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "order by ltimestamp ";

                                StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                        "from rzpali.taxilogp " +
                                        "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                        "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                        "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                        "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                        "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                        "order by ltimestamp ";
                                DBC.Q_Run3(StrSql);
                                string ProdBefore = "";
                                string ProdAfter = "";
                                string Selected = "";
                                while (DBC.Rs3.Read())
                                {
                                    if (ProdBefore == "")
                                    {
                                        ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                                    }
                                    else
                                    {
                                        ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                                    }
                                }
                                // בחירת הפריט שישובץ
                                if (ProdBefore == ProdAfter)
                                {
                                    xlWorkSheet.Cells[i, 9] = ProdBefore;
                                    Selected = ProdBefore;
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i, 9] = ProdAfter;
                                    Selected = ProdAfter;
                                }

                                //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "from rzpali.taxilogp " +
                                //         "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                //         "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                //         "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                //         "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                //         "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                //         "order by ltimestamp ";
                                StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1,insiz " +
                                        "from rzpali.taxilogp " +
                                        "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                        "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                        "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                                        "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                        "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                        "group by ltimestamp,lprod,rmtkn,ICSCP1,insiz " +
                                        "order by ltimestamp ";
                                DBC.Q_Run3(StrSql);
                                while (DBC.Rs3.Read())
                                {
                                    if (DBC.Rs3.GetValue(1).ToString().Trim() == Selected)
                                    {
                                        try
                                        {
                                            if (DBC.Rs3.GetValue(2).ToString().Trim() != "")
                                            {
                                                xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(2).ToString().Trim();
                                                xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                                xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(3) * (Convert.ToInt64((DBC.Rs2.GetDateTime(1) - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(2), 2); // משקל שאבד            
                                                xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(4).Trim();
                                                newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                            }

                                            xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(3), 2);
                                        }
                                        catch (Exception e)
                                        {
                                            MessageBox.Show(e.Message + "   " + e.ToString());
                                            throw;
                                        }


                                    }
                                }

                                StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                                DBC.Q_Run3(StrSql);
                                if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                                {
                                    xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i, 14] = "1";
                                }

                                i++;
                                ///////////////////////////////////////////////////////////////////////
                                StopStt = "0";
                                LastTak = "";
                                TakCode = "";
                                TakDesc = "";
                                EmpNo = "";
                                EmpName = "";
                                if (FlgBeforDt)
                                {   // Cell Back Color
                                    chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                                    chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                                }
                            }
                        }
                    }
                    // בדיקת מצב שהיה דיווח תקלה, והתקלה לא הסתיימת בתקופת הדוח
                    if (StopStt == "1")
                    {
                        xlWorkSheet.Cells[i, 1] = LastPress; // מכבש
                        xlWorkSheet.Cells[i, 2] = StartStopDT; // שעת התחלה
                                                               // בדיקה אם תאריך סיום דוח הוא יום נוכחי, במידה וכן אז שעת סיום תקלה נכון לעכשיו

                        // כוכב כי תאריך סוף הדוח תמיד יהיה היום
                        //if (DTPicker_To.Value.Date == DateTime.Now.Date)
                        //{
                        //xlWorkSheet.Cells[i, 3] = DateTime.Now; // שעת סיום נכון לעכשיו
                        //xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DateTime.Now - StartStopDT).TotalMinutes); // סה''כ זמן
                        xlWorkSheet.Cells[i, 3] = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                        xlWorkSheet.Cells[i, 4] = Convert.ToInt64((DateTime.Now - StartStopDT).TotalMinutes);// סה''כ זמן
                                                                                                             // Cell Back Color
                        chartRange = xlWorkSheet.get_Range("C" + i.ToString(), "C" + i.ToString());
                        chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.OrangeRed);
                        //}
                        //else
                        //{
                        //    xlWorkSheet.Cells[i, 3] = Convert.ToDateTime(DTPicker_To.Value.ToString("dd/MM/yyyy HH:mm")); // שעת סיום
                        //    xlWorkSheet.Cells[i, 4] = Convert.ToInt64((Convert.ToDateTime(DTPicker_To.Value.ToString("dd/MM/yyyy HH:mm")) - StartStopDT).TotalMinutes); // סה''כ זמן
                        //    // Cell Back Color
                        //    chartRange = xlWorkSheet.get_Range("C" + i.ToString(), "C" + i.ToString());
                        //    chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                        //}
                        xlWorkSheet.Cells[i, 5] = EmpNo; // מס' עובד
                        xlWorkSheet.Cells[i, 6] = EmpName; // שם עובד                                   
                        xlWorkSheet.Cells[i, 7] = TakCode; // קוד תקלה
                        xlWorkSheet.Cells[i, 8] = TakDesc; // תיאור תקלה

                        //// תוספות מתאריך 09/11/2016 בוצע ע"י רן הויכמן
                        //StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                        //                 "from rzpali.taxilogp " +
                        //                 "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                        //                 "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                        //                 "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                        //                 "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + DBC.Rs1.GetDateTime(1).ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                        //                 "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                        //                 "order by ltimestamp ";

                        StrSql = "select ltimestamp,lprod,rmtkn,ICSCP1 " +
                                        "from rzpali.taxilogp " +
                                        "left join bpcsfali.frtml01 on RMPROD=lprod and RMMACI=lpress  " +
                                        "left join bpcsfv30.cicl01 on ICPROD=lprod and ICFAC='F1' " +
                                        "where lpress='" + DBC.Rs1.GetString(0).ToString().Trim() + "' and (ltimestamp = (select max(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp<'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "') OR " +
                                        "ltimestamp = (select min(ltimestamp) from rzpali.taxilogp where lpress='" + DBC.Rs1.GetString(0).Trim() + "' and lact='0' and ltimestamp>'" + StartStopDT.ToString("yyyy-MM-dd-HH.mm.ss.000000") + "')) " +
                                        "group by ltimestamp,lprod,rmtkn,ICSCP1 " +
                                        "order by ltimestamp ";
                        DBC.Q_Run3(StrSql);
                        string ProdBefore = "";
                        string ProdAfter = "";
                        string Selected = "";
                        while (DBC.Rs3.Read())
                        {
                            if (ProdBefore == "")
                            {
                                ProdBefore = DBC.Rs3.GetString(1).ToString().Trim();
                            }
                            else
                            {
                                ProdAfter = DBC.Rs3.GetString(1).ToString().Trim();
                            }
                        }
                        // בחירת הפריט שישובץ
                        if (ProdAfter == "")
                        {
                            StrSql = "select izprod,izqreq,izqprd,IZREQ " +
                                        "from bpcsfali.shmal01 " +
                                        "where IZDEPT='" + DBC.Rs1.GetValue(2).ToString().Trim() + "' and  IZMACH='" + DBC.Rs1.GetValue(0).ToString().Trim() + "' ";
                            DBC.Q_Run3(StrSql);
                            while (DBC.Rs3.Read())
                            {
                                try
                                {
                                    if (DBC.Rs3.GetValue(0).ToString().Trim() != "STOP")
                                    {
                                        if (DBC.Rs3.GetDecimal(1) > DBC.Rs3.GetDecimal(2))
                                        {
                                            ProdAfter = DBC.Rs3.GetValue(0).ToString().Trim();
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        for (int l = 0; l < 20; l++)
                                        {
                                            if (ArrErrCode[l] == DBC.Rs3.GetDecimal(3))
                                            {
                                                ProdAfter = DBC.Rs3.GetValue(0).ToString().Trim();
                                                break;
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(e.Message + "   " + e.ToString());
                                    throw;
                                }


                            }
                        }
                        if (ProdBefore == ProdAfter)
                        {
                            xlWorkSheet.Cells[i, 9] = ProdBefore;
                            Selected = ProdBefore;
                        }
                        else
                        {
                            xlWorkSheet.Cells[i, 9] = ProdAfter;
                            Selected = ProdAfter;
                        }
                        StrSql = "select  RMPROD,rmtkn,ICSCP1,insiz " +
                                 "from  bpcsfali.frtml01 " +
                                 "left join bpcsfv30.cicl01 on ICPROD=RMPROD " +
                                 "left join BPCSFALI.iimnl01 on substring(rmprod,1,8)=substring(inprod,1,8) " +
                                 "where RMPROD='" + Selected.Trim() + "' and RMMACI='" + DBC.Rs1.GetString(0).Trim() + "' ";
                        DBC.Q_Run3(StrSql);
                        while (DBC.Rs3.Read())
                        {
                            if (DBC.Rs3.GetValue(0).ToString().Trim() == Selected.Trim())
                            {
                                try
                                {
                                    xlWorkSheet.Cells[i, 10] = DBC.Rs3.GetDecimal(1).ToString().Trim();
                                    xlWorkSheet.Cells[i, 11] = Math.Round(DBC.Rs3.GetDecimal(2), 2);
                                    xlWorkSheet.Cells[i, 12] = Math.Round(DBC.Rs3.GetDecimal(1) * (Convert.ToInt64((DateTime.Now - StartStopDT).TotalMinutes)) / 1440, 2); // זמן תקלה
                                    xlWorkSheet.Cells[i, 13] = Math.Round((DBC.Rs3.GetDecimal(2) * (Convert.ToInt64((DateTime.Now - StartStopDT).TotalMinutes)) / 1440) / 1000 * DBC.Rs3.GetDecimal(1), 2); // משקל שאבד               
                                    xlWorkSheet.Cells[i, 15] = DBC.Rs3.GetString(3).Trim();
                                    newtime = (StartStopDT.Hour * 100 + StartStopDT.Minute).ToString();
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(e.Message + "   " + e.ToString());
                                    throw;
                                }

                            }

                        }

                        StrSql = "select sshft from BPCSFALI.shf where SDAY=0 and " + newtime + " between sfhrs and sthrs ";
                        DBC.Q_Run3(StrSql);
                        if (DBC.Rs3.Read() && !DBC.Rs3.IsDBNull(0))
                        {
                            xlWorkSheet.Cells[i, 14] = DBC.Rs3.GetValue(0).ToString().Trim();
                        }
                        else
                        {
                            xlWorkSheet.Cells[i, 14] = "1";
                        }
                        ///////////////////////////////////////////////////////////////////////

                        if (FlgBeforDt)
                        {   // Cell Back Color
                            chartRange = xlWorkSheet.get_Range("B" + i.ToString(), "B" + i.ToString());
                            chartRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                        }
                        i++;
                    }
                    LastPress = "";
                    StopStt = "0";
                    LastTak = "";
                    TakCode = "";
                    TakDesc = "";
                    EmpNo = "";
                    EmpName = "";


                }
                DBC.Rs1.Close();
                DBC.Rs2.Close();
                DBC.Rs3.Close();
                DBC.Close_Conn();

                //System.IO.File.Delete(@"\\qlikview\e$\Qlikview\QlikView Input\Faults.xlsx");
                xlApp.DisplayAlerts = false;
                //xlWorkBook.SaveAs(@"\\qlikview\e$\Qlikview\QlikView Input\Faults.xlsx",Type.Missing, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlShared, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.SaveAs(@"\\qlikview\e$\Excel Files\Faults\Faults.xlsx");
                //xlWorkBook.SaveAs(@"C:\C#\Faults.xlsx");
                xlApp.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                Cursor.Current = Cursors.PanEast;
                Application.Exit();
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}




