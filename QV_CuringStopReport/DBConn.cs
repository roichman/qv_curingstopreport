﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;

namespace QV_CuringStopReport
{
    class DBConn
    {
        string connectionString; 
        OdbcConnection conn;
        OdbcCommand command;

        public OdbcDataReader Rs1;
        public OdbcDataReader Rs2;
        public OdbcDataReader Rs3;

        public void Initialize_Conn(string Str_Conn)
        {
            connectionString = Str_Conn;
            conn = new OdbcConnection(connectionString);            
            conn.Open();  
        }

        public bool Q_Run1(string Sql_Str)
        {
            command = new OdbcCommand(Sql_Str, conn);
            command.CommandTimeout = 25000;               
            Rs1 = command.ExecuteReader();
            try { return true;}
            catch { return false;} 
        }

        public bool Q_Run2(string Sql_Str)
        {
            command = new OdbcCommand(Sql_Str, conn);
            command.CommandTimeout = 1000;
            Rs2 = command.ExecuteReader();
            try { return true; }
            catch { return false; }
        }

        public bool Q_Run3(string Sql_Str)
        {
            command = new OdbcCommand(Sql_Str, conn);
            command.CommandTimeout = 10000;
            Rs3 = command.ExecuteReader();
            try { return true; }
            catch { return false; }
        }

        public bool Run(string Sql_Str)
        {
            command = new OdbcCommand(Sql_Str, conn);
            command.CommandTimeout = 1000;
            command.ExecuteNonQuery();  
            try { return true; }
            catch { return false; }
        }
        public void Close_Conn()
        {
            conn.Close();
        }
    }
}
